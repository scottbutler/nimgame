*************************************************
*                                               *
*               Welcome to NIM                  *
*               by Scott Butler                 *
* --------------------------------------------  *
*                                               *
* GAME INSTRUCTIONS:                            *
* These are the rules for the NIM game:         *
* There are two players, A and B.               *
* There is a pile of pebbles on the table.      *
*                                               *
* Player A starts, taking some pebbles;         *
* he/she must take at least one pebble,         *
* and must leave at least one on the table.     *
*                                               *
* Player B then takes some pebbles; B must      *
* take at least one, but cannot take more than  *
* the number of pebbles player A took; and      *
* obviously player B cannot take more than the  *
* number of the pebbles left on the table.      *
*                                               *
* In general, a player can take any number of   *
* pebbles between 1 and the number that the     *
* previous player took, but obviously no more   *
* than the number of pebbles left on the table. *
*                                               *
* Player to take the last pebble wins.          *
*                                               *
* In this version, you are versing NIMbot.      *
*                                               *
*************************************************


* INSTRUCTIONS
0. Optional, to rebuild: mvn clean package
1. Start: java -jar assign.jar
2. Enter a pebble count (minimum is 2 or greater)
3. Enter 3 to start playing the name
4. Enter your name
5. You are now playing NIM. Firstly, enter how many pebbles you wish to take.
6. Continue to follow the onscreen instructions, until the game ends.



* MAIN MENU (options)
1. Show NIM graph
2. Show graph labels (winning/losing)
3. PLAY the NIM game
4. Change initial pebble count
5. Exit



* FURTHER INFORMATION
Menu Option 1: This option will print out the ordered version of the graph 
if it was previously sorted, else the unordered graph.

Menu Option 4: The program will notify the user that changing the initial 
pebble count will require the game to immediately reload upon confirmation; 
loosing the current game state (maintaining menu consistency).



* IMPLEMENTATION
The generation of the state graph uses Breath-First search to determine
all possible states in the most efficent way possible. This allows for the
generation of a graph with 1000 pebbles, with each state labelled, 
completed in 15 seconds. (With n=80 pebbles, the average time is 2 milliseconds over 250 trials!)
The labelling (state success: win/lose) is implemented using a topologically sorted
list, which is then iterated upon in the reverse order (using a descending iterator for efficiency).

For the storing of the graph, there exists a Hashtable in GraphNIM which 
contains every node of the graph, representing a possible state of the game.
Each node contains a adjacency list (storded as a LinkedList) which 
contains references to all neighbour nodes (states).
This Hashtable implementation allows for constant time O(1) access to a vertex which 
increases the performance of the graph generation, by allowing for 
instant checking as to whether a node already exists in the graph. 
Unlike a LinkedList, which requires traversal over all nodes to verifiy whether it exists.

During Topological sort, the next state to be inserted into the output, 
will be added to the start of a LinkedList, preventing the need for the 
list to be reversed once the sort completes -- reducing operational complexity.



* BONUS FEATURES (AND CUSTIMISATION)
This version of NIM has a user player versing NIMbot (an automated bot designed to always win).
To disable NIMbot from always winning, thus allowing for a fair game, 
enter the sequence 333, to toggle the autobot feature. 
NB: this setting resets after each game (or regeneration).
The default setting is defined in NIMbot.java on line ~30 as boolean field botCheat.

The pebble symbol can be changed inside the NIMGameboard.java on line ~29. 
The string field PEBBLE is the symbol used, and default is "O".

Change minimum pebble count by changing the int field of MIN_PEBBLE_COUNT in NIMGameboard.java on line 28.

NIMbot can shuffle the states before choosing which move to take.
This results in a different game experience when the pebble count remains the same.
To toggle this setting, change the boolean field SHUFFLE in NIMbot.java on line ~27.
NB: this is enabled by default when botCheat is disabled.



* TESTING PERFORMANCE
Timed testing of graph generation and labelling can be achieved 
by enter the sequence 666, when within the main menu.
The testing depends on two values: 
trials (how many times to run the calculation); testPebbleAmount (how many pebbles to use).
These values can be changed in DemoGame.java on line ~137.
The average is then computed at the end, and displayed in the console.