package me.scottbutler.NIMS;

import me.scottbutler.NIMS.exceptions.GameOverException;
import me.scottbutler.NIMS.exceptions.IllegalMoveException;
import me.scottbutler.NIMS.model.GraphNIM;
import me.scottbutler.NIMS.model.ListNIM;
import me.scottbutler.NIMS.model.NodeNIM;

import java.util.Iterator;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 * ...
 * ...
 */

public class NIMGameboard {
    /**
     * Minimum pebble count for start of a game.
     */
    private static final int MIN_PEBBLE_COUNT = 2;

    /**
     * The pebble symbol to use. Suggested value "O"
     */
    private static final String PEBBLE = "O";


    private int initialPebbleCount;
    private GraphNIM gameStateGraph;
    private NIMbot AIbot;
    private NIMPlayer userPlayer;

    // true if AI went first
    private boolean AIbotMovedFirst;
    //winner of game
    private NIMPlayer winner;
    private boolean gameEnded;

    public NIMGameboard(int _initialPebbleCount) {
        initialPebbleCount = _initialPebbleCount;

        // create graph
        gameStateGraph = new GraphNIM();

        // user
        // create bot player
        this.AIbot = new NIMbot(this.gameStateGraph);
        this.userPlayer = new NIMPlayer(this.gameStateGraph, "User Player");

        // game not over
        gameEnded = false;
        winner = null;
    }

    /* begin: GETTERS and SETTERS */

    public static int getMIN_PEBBLE_COUNT() {
        return MIN_PEBBLE_COUNT;
    }

    public int getInitialPebbleCount() {
        return initialPebbleCount;
    }

    public String printGameGraph() {
        return this.gameStateGraph.toString();
    }
    public GraphNIM getGameStateGraph() {
        return gameStateGraph;
    }

    /* end: GETTERS and SETTERS */

    /**
     * Gets the name of the AIbot
     * @return name of bot
     */
    public String getBotsName() {
        return this.AIbot.getPlayerName();
    }

    /**
     * Checks if there is a winner for the game, if so,
     * returns the reference to them, otherwise returns no winner.
     * @return player representing winner
     */
    public NIMPlayer getWinner() {
        if (gameEnded && this.winner == null) {
            return new NIMPlayer(this.gameStateGraph, "NO WINNER");
        }
        return winner;
    }

    /**
     * Start game. Sets the name of the user, checks if AIbot is to go first,
     * then makes a move for AIbot if desired.
     *
     * @param userPlayersName name of user
     */
    public void startGame(String userPlayersName) throws IllegalMoveException {
        // update players name
        this.userPlayer.setPlayerName(userPlayersName);

        // let bot decide who to go first
        if (this.AIbot.willGoFirst()) {
            // take X pebbles
            try {
                this.AIbot.move();
            } catch (GameOverException e) {
                this.gameoverSetWinner(AIbot);
            }
            this.AIbotMovedFirst = true;
        } else {
            // let opponent go first
            this.AIbotMovedFirst = false;
        }
    }

    /**
     * Sets the winner of the game, and sets the game as ended.
     * @param _winner the player who won
     */
    private void gameoverSetWinner(NIMPlayer _winner) {
        this.gameEnded = true;
        this.winner = _winner;
    }

    // getter
    public boolean AIbotWentFirst() {
        return AIbotMovedFirst;
    }

    /**
     * Fetches the last amount of pebbles taken by a player.
     * If no pebbles were taken, then value will be 0.
     * @return the amount taken
     */
    public int pebblesLastTaken() {
        return this.gameStateGraph.getPebblesLastTaken();
    }

    /**
     * Populate the graph with a node for each state of the game.
     * Does not determine the success of each state.
     */
    public void generateGraph() {
        // ensure valid
        assert (this.initialPebbleCount >= MIN_PEBBLE_COUNT);

        // initial values
        int n_pebbleCount = this.initialPebbleCount;
        // Player A starts, taking k pebbles, where 1 <= k < n .
        int k_pebblesAvailableToTake = n_pebbleCount - 1;

        // add entry nodes
        NodeNIM rootNode = new NodeNIM(n_pebbleCount, k_pebblesAvailableToTake);
        gameStateGraph.add(rootNode);
        // set as active state
        this.gameStateGraph.start(rootNode);

        // generate all possible states
        gameStateGraph.generateGraph();
    }

    /**
     * Determines for each node whether it is a winning or losing state.
     */
    public void determineStateSuccess() {
        // get sorted graph as linkedlist -- calculates list if not previously done
        ListNIM sortedNodes = this.gameStateGraph.getToposortedNodes();

        // determine success for each state
        // use reverse order of list to start at node with no dependencies
        Iterator<NodeNIM> reversedItr = sortedNodes.descendingIterator();
        while (reversedItr.hasNext()) {
            NodeNIM current = reversedItr.next();

            if (current.isTerminal()) {
                // mark terminal nodes as losing nodes
                current.setLosing();
            } else if (current.hasLosingNeighbour()) {
                // winning state (nonterminal with any edge leading to a losing node)
                current.setWinning();
            } else if (current.hasAllWinningNeighbours()) {
                // losing state (nonterminal with all edges leading to winning states)
                current.setLosing();
            }
        }
    }

    /**
     * Returns a string of the status of the table,
     * and shows how many pebbles are on the board
     *
     * @return string representation of the game
     */
    public String boardStatus() {
        StringBuilder pebbles = new StringBuilder();
        int pebblesRemaining = this.getPebblesRemaining();

        for (int i = 0; i < pebblesRemaining; i++) {
            pebbles.append(PEBBLE);
        }

        if (pebblesRemaining == 0) {
            return "There are no more pebbles on the table.";
        }

        return "There " + ((pebblesRemaining < 2) ? "is " : "are ") + pebblesRemaining + " pebble" + ((pebblesRemaining < 2) ? "" : "s")
            + " on the table: "
            + pebbles.toString();
    }

    /**
     * Fetches the pebbles remaining for the active (current) state of the game.
     * @return amount of pebbles on table
     */
    public int getPebblesRemaining() {
        return this.gameStateGraph.getActiveState().getPebblesRemaining();
    }

    @Override
    public String toString() {
        return this.gameStateGraph.toString();
    }

    /**
     * Assumes graph has been sorted topologically previously.
     * Shows whether each state is winning, losing, or undecided.
     *
     * @return string representation of all possible states and their success labels
     */
    public String getGameStateSuccessGraph() {
        StringBuilder overview = new StringBuilder();

        this.gameStateGraph.getToposortedNodes().stream().forEach(current -> {
            // key name
            overview.append(current.getKey()).append(": ");

            // state success
            if (current.isWinning()) {
                overview.append("Winning");
            } else if (current.isLosing()) {
                overview.append("Losing");
            } else {
                overview.append("Undecided");
            }

            overview.append("\n");
        });

        return overview.toString();
    }

    /**
     * Check if more pebbles allowed to take (i.e. moves remain)
     *
     * @return true if more pebbles remain
     */
    public boolean pebblesRemain() {
        return (!this.gameStateGraph.getActiveState().isTerminal());
    }

    // getter
    public boolean gameover() {
        return (this.gameEnded);
    }

    /**
     * The AIbot will attempt to make a move.
     * @throws IllegalMoveException bot made invalid or illegal move. (system error)
     */
    public void playBotMove() throws IllegalMoveException {
        try {
            this.AIbot.move();

            // check for game over -- if i just moved to this state, then i won
            if (this.gameStateGraph.getActiveState().isTerminal()) {
                this.gameEnded = true;
                this.winner = AIbot;
            }
        } catch (GameOverException e) {
            // game already over
            if (!this.gameEnded) {
                this.gameEnded = true;
            }
        }
    }

    /**
     * Attempts to take an amount of pebbles from the table.
     * @param pebblesToTake amount to take
     * @throws IllegalMoveException invalid selection of pebbles
     */
    public void playUserMove(int pebblesToTake) throws IllegalMoveException {
        try {
            this.userPlayer.move(pebblesToTake);

            // check for game over -- if i just moved to this state, then i won
            if (this.gameStateGraph.getActiveState().isTerminal()) {
                this.gameEnded = true;
                this.winner = userPlayer;
            }
        } catch (GameOverException e) {
            // game already over
            if (!this.gameEnded) {
                this.gameEnded = true;
            }
        }
    }

    /**
     * Toggle autobot cheating. When disabled AIbot will take random moves, else will always win.
     */
    public void toggleAutobot() {
        if (this.AIbot.isBotCheat()) {
            this.AIbot.setBotCheat(false);
        } else {
            this.AIbot.setBotCheat(true);
        }
    }

    /* begin: GETTERS and SETTERS */

    public boolean isBotCheating() {
        return this.AIbot.isBotCheat();
    }

    public String getUsersName() {
        return this.userPlayer.getPlayerName();
    }

    /* end: GETTERS and SETTERS */
}
