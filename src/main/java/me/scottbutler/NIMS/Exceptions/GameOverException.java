package me.scottbutler.NIMS.exceptions;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  This exception represents the intention for the game to end,
 *  useful in the case of the game ending, yet unexpectedly, a user tried to
 *  move (take pebbles). Though, could be thrown if a user wins, to indicate
 *  the end of a game gracefully.
 */

public class GameOverException extends Exception {
    public GameOverException(String message) {
        super(message);
    }

    public GameOverException() {
        super("Gameover, there are no moves left.");
    }
}
