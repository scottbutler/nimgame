package me.scottbutler.NIMS.exceptions;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  Represents the situations in which a NIMPlayer tried to make a invalid
 *  or illegal move. Such as, taking more pebbles than possible.
 */

public class IllegalMoveException extends Exception {
    public IllegalMoveException() {
        super("Cannot move to requested state.");
    }

    public IllegalMoveException(String message) {
        super(message);
    }
}
