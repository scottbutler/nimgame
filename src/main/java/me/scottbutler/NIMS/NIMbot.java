package me.scottbutler.NIMS;

import me.scottbutler.NIMS.exceptions.GameOverException;
import me.scottbutler.NIMS.exceptions.IllegalMoveException;
import me.scottbutler.NIMS.model.GraphNIM;
import me.scottbutler.NIMS.model.NodeNIM;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  NIMbot class represents the smart AI bot that verses the user, and thus
 *  is a type of NIMPlayer. NIMbot has extended abilities to determine
 *  which states (pebbles to take) it should transition to [to win].
 *  This class has extra ability to shuffle choices.
 */

public class NIMbot extends NIMPlayer {

    /**
     NIMbot can shuffle the states before choosing which move to take.
     This results in a different game experience when the pebble count remains the same.
     To toggle this setting, change the boolean field SHUFFLE in NIMbot.java on line 27.
     NB: this is enabled by default when botCheat is disabled.
     */
    // when true will shuffle neighbours before choosing next move
    private static final boolean SHUFFLE = true;

    /**
     * This version of NIM has a user player versing NIMbot (an automated bot designed to always win).
     To disable NIMbot from always winning, thus allowing for a fair game,
     enter the sequence 333, to toggle the autobot feature.
     NB: this setting resets after each game (or regeneration).
     The default setting is defined in NIMbot.java on line ~30 as boolean field botCheat.
     */
    // when true, will always win
    private boolean botCheat = true;

    // reference to game graph

    public NIMbot(GraphNIM gameStateGraph) {
        super(gameStateGraph, "NIMbot");
    }

    /* begin: GETTERS and SETTERS */

    public boolean isBotCheat() {
        return botCheat;
    }

    public void setBotCheat(boolean botCheat) {
        this.botCheat = botCheat;
    }

    /* end: GETTERS and SETTERS */

    /**
     * Determines whether the NIMbot will take the first or second move in order to win the game.
     * Based on whether the first state is a winning state.
     * @return boolean of whether NIMbot will take first turn. True means yes.
     */
    public boolean willGoFirst() {
        // if node (n, n-1) is winning, then go first
        return (this.gameGraph.getActiveState().isWinning());
    }

    /**
     * Custom algorithm that decides which move to take by the AIbot.
     * If AIbot is allowed to cheat (botCheat=true) then will always choose a move that
     * results in the active state being a winning node.
     * Otherwise, will choose a random move.
     * When shuffle is enabled and botCheat is true, then the AIbot will take a random move that still
     * results in the AIbot winning. This is useful when the user plays the game multiple times with
     * the same initial pebble count, to give the illusion of random choice.
     * @throws GameOverException bot cant move for no moves remain, game has ended
     * @throws IllegalMoveException bot tried moving to invalid state (system error)
     */
    public void move() throws GameOverException, IllegalMoveException {
        // no moves left
        if (this.gameGraph.noMovesLeft()) {
            // game over
            throw new GameOverException();
        }
        // can only move one way
        else if (this.gameGraph.getActiveState().getNeighbours().size() == 1) {
            // next state
            NodeNIM nextState = this.gameGraph.getActiveState().getNeighbours().getFirst();
            // take move
            this.gameGraph.moveToState(nextState);
            return;
        }

        if (!this.botCheat) {
            // shuffles and picks one
            this.gameGraph.moveToRandomState();
            return;
        }

        // check to shuffle neighbours
        if (SHUFFLE) {
            this.gameGraph.getActiveState().shuffleNeighbours();
        }

        // cheat to win
        if (this.botCheat) {
            // chose next losing state
            for (NodeNIM possibleMove : this.gameGraph.getActiveState().getNeighbours()) {
                // look for losing neighbour to send opponent to
                if (possibleMove.isLosing()) {
                    this.gameGraph.moveToState(possibleMove);
                    return;
                }
            }

            // no losing states to move to -- check if neighbour after current neighbour is wining
            for (NodeNIM possibleMove : this.gameGraph.getActiveState().getNeighbours()) {
                if (possibleMove.hasWinningNeighbour()) {
                    // move to it
                    this.gameGraph.moveToState(possibleMove);
                }
            }
        }

        // last resort take any move
        this.gameGraph.moveToAnyState();
    }
}
