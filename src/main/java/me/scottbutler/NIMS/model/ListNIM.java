package me.scottbutler.NIMS.model;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  This class represents the list of adjacent (neighbour) nodes of the game graph.
 *  Providing wrapper functionality such a shuffling the order of neighbours,
 *  and a toString method.
 */

public class ListNIM extends LinkedList<NodeNIM> {
    public ListNIM() {
    }

    public ListNIM(Collection<? extends NodeNIM> c) {
        super(c);
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();

        for (NodeNIM current : this) {
            output.append(current.getKey()).append(": ").append(current.getNeighboursString()).append("\n");
        }

        return output.toString();
    }

    /**
     * Shuffles the order of nodes in the list.
     * Useful for random moves.
     */
    public void shuffle() {
        // shuffles order of list
        Collections.shuffle(this);
    }
}
