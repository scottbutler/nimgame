package me.scottbutler.NIMS.model;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  This class represents the nodes (or states) of the graph (game).
 *  These states have success labels which describe the potential
 *  outcome of the game once a user reaches the state.
 *  GraphNIM stores instances of this class NodeNIM.
 */

public class NodeNIM implements Comparable<NodeNIM> {
    // not yet decided
    private static final int STATE_OUTCOME_UNDECIDED = 0;
    // is loosing state
    private static final int STATE_OUTCOME_LOSE = 1;
    // is winning state
    private static final int STATE_OUTCOME_WIN = 2;
    private String stateKey;
    private int pebblesRemaining;
    private int pebblesAllowedToTake;
    // -1 init
    private int pebblesLastTaken = -1;
    // for searching
    private boolean visited;
    private int stateOutcome;

    private ListNIM neighbours;

    public NodeNIM(int _pebblesRemaining, int _pebblesAllowedToTake) {
        // create key
        this.stateKey = createStateKeygen(_pebblesRemaining, _pebblesAllowedToTake);
        this.visited = false;
        this.pebblesRemaining = _pebblesRemaining;
        this.pebblesAllowedToTake = _pebblesAllowedToTake;
        this.neighbours = new ListNIM();
        this.stateOutcome = STATE_OUTCOME_UNDECIDED;
    }

    /* begin: GETTERS and SETTERS */

    public static String createStateKeygen(int pebblesRemaining, int pebblesAllowedToTake) {
        return "(" + pebblesRemaining + "," + pebblesAllowedToTake + ")";
    }

    public int getPebblesRemaining() {
        return pebblesRemaining;
    }

    public void setPebblesRemaining(int _pebblesRemaining) {
        this.pebblesRemaining = _pebblesRemaining;
    }

    public int getPebblesLastTaken() {
        return pebblesLastTaken;
    }

    public void setPebblesLastTaken(int _pebblesLastTaken) {
        this.pebblesLastTaken = _pebblesLastTaken;
    }

    public int getPebblesAllowedToTake() {
        return pebblesAllowedToTake;
    }

    public void setPebblesAllowedToTake(int _pebblesAllowedToTake) {
        this.pebblesAllowedToTake = _pebblesAllowedToTake;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean _visited) {
        this.visited = _visited;
    }

    public boolean isUndecided() {
        return (this.stateOutcome == STATE_OUTCOME_UNDECIDED);
    }

    public void setWinning() {
        this.stateOutcome = STATE_OUTCOME_WIN;
    }

    public void setLosing() {
        this.stateOutcome = STATE_OUTCOME_LOSE;
    }

    /* end: GETTERS and SETTERS */

    /**
     * Checks if the node has no exit paths
     *
     * @return true if dead end
     */
    public boolean isDeadNode() {
        return (this.pebblesRemaining < 1);
    }

    /**
     * Merges a list of nodes with the existing list of neighbours, ignoring any that already exist.
     * @param adjacentNodes list to merge from.
     */
    public void addNeighbours(LinkedList<NodeNIM> adjacentNodes) {
        // merge (dont add if already in neighbours list)
        adjacentNodes.stream().filter(current -> !this.neighbours.contains(current)).forEach(
            this.neighbours::add
        );
    }

    /**
     * Add a neighbour node if not already neighbour
     * @param neighbourNode new neighbour
     */
    public void addNeighbour(NodeNIM neighbourNode) {
        if (!this.neighbours.contains(neighbourNode)) {
            this.neighbours.add(neighbourNode);
        }
    }

    /**
     * Represents neighbours as string
     * @return string representation
     */
    public String getNeighboursString() {
        StringBuilder output = new StringBuilder();

        for (Iterator<NodeNIM> itr = this.neighbours.iterator(); itr.hasNext(); ) {
            NodeNIM current = itr.next();

            output.append(current.getKey());

            // divider
            if (itr.hasNext()) {
                output.append(", ");
            }
        }

        return output.toString();
    }

    // getter
    public String getKey() {
        return this.stateKey;
    }

    @Override public int hashCode() {
        return stateKey.hashCode();
    }

    /**
     * Matches two nodes if the share the same key, which is a sequence of pebbles on the table, and the max amount allowed to be taken.
     * @param o is a node to compare to
     * @return boolean value, true if two objects are identical
     */
    @Override public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NodeNIM that = (NodeNIM) o;

        // properties ignored
        //        if (this.pebblesAllowedToTake != that.getPebblesAllowedToTake() || this.getPebblesRemaining() != that.getPebblesRemaining() || this.isVisited() != that.isVisited()) {
        //            return false;
        //        }

        return stateKey.equals(that.stateKey);

    }

    /**
     * Compares the given "that" node to check the order relative to this node.
     * @param that is a node to compare this to
     * @return int value for whether that node is before, equal to, or comes after this node.
     */
    @Override public int compareTo(NodeNIM that) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (this == that) {
            return EQUAL;
        }

        // if exists edge from this to that, then this comes before that
        if (this.neighbours.contains(that)) {
            return BEFORE;
        } else if (that.getNeighbours().contains(this)) {
            return AFTER;
        } else {
            return EQUAL;
        }
    }

    // getter
    public ListNIM getNeighbours() {
        return neighbours;
    }

    /**
     * Determines whether node has no exit paths, thus a terminal
     *
     * @return true when this node is deadend.
     */
    public boolean isTerminal() {
        return this.neighbours.isEmpty();
    }

    /**
     * Determines if this node has any losing neighbours.
     * @return true for at least one
     */
    public boolean hasLosingNeighbour() {
        for (NodeNIM current : this.neighbours) {
            if (current.isLosing()) {
                return true;
            }
        }

        // no losing neighbours
        return false;
    }

    /**
     * Checks if this node is losing based on the pre-determined state outcome.
     * @return true for losing node
     */
    public boolean isLosing() {
        return (this.stateOutcome == STATE_OUTCOME_LOSE);
    }

    /**
     * Determines if this node has all winning neighbours.
     * @return true for node has all winning nodes OR when there are no neighbours.
     */
    public boolean hasAllWinningNeighbours() {
        for (NodeNIM current : this.neighbours) {
            if (!current.isWinning()) {
                return false;
            }
        }

        // all wining nodes
        return true;
    }

    /**
     * Checks if this node is winning based on the pre-determined state outcome.
     * @return true for winning node
     */
    public boolean isWinning() {
        return (this.stateOutcome == STATE_OUTCOME_WIN);
    }

    /**
     * Determines if this node has any wining neighbours.
     * @return true if at least one
     */
    public boolean hasWinningNeighbour() {
        for (NodeNIM current : this.neighbours) {
            if (current.isWinning()) {
                return true;
            }
        }

        // no winning neighbours
        return false;
    }

    /**
     * Shuffles the order of this node's neighbours.
     */
    public void shuffleNeighbours() {
        this.neighbours.shuffle();
    }
}
