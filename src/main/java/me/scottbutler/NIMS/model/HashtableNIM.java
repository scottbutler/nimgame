package me.scottbutler.NIMS.model;

import java.util.Hashtable;
import java.util.Map;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  HashtableNIM is the main data structure for the nodes inside GraphNIM.
 *  Nodes are added to this hashtable, allowing for instant insertion/deletion/searching O(1)
 *  This class acts a wrapper providing extra functionality such as a toString method.
 */

public class HashtableNIM extends Hashtable<String, NodeNIM> {
    public HashtableNIM(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public HashtableNIM(int initialCapacity) {
        super(initialCapacity);
    }

    public HashtableNIM() {
        super();
    }

    public HashtableNIM(Map<? extends String, ? extends NodeNIM> t) {
        super(t);
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();

        for (NodeNIM current : this.values()) {
            output.append(current.getKey()).append(": ").append(current.getNeighboursString()).append("\n");
        }

        return output.toString();
    }
}
