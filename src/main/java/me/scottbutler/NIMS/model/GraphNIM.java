package me.scottbutler.NIMS.model;

import me.scottbutler.NIMS.exceptions.GameOverException;
import me.scottbutler.NIMS.exceptions.IllegalMoveException;

import java.util.LinkedList;
import java.util.Queue;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  This class represents the main Graph of the game,
 *  representing the states of the game, and the
 *  potential success of each state.
 *  This class provides the searching and sorting of the
 *  graph, including topological sort, breadth-first search,
 *  labelling state success, and calculating neighbours of states.
 *  GraphNIM stores instances of NodeNIM.
 */

public class GraphNIM {

    // Initial space allocated for adjacency list (for n=pebbles=5, size=states=8)
    private static final int INITIAL_HASHMAP_SIZE = 8;

    private HashtableNIM adjacencyList;

    // done only when needed. list of ordered nodes -- USE GETTER
    private ListNIM toposortedNodes = null;

    // the current state (node) of the game
    private NodeNIM activeState;

    public GraphNIM() {
        this.adjacencyList = new HashtableNIM(INITIAL_HASHMAP_SIZE);
    }

    public HashtableNIM getAdjacencyList() {
        return adjacencyList;
    }

    public NodeNIM getActiveState() {
        return activeState;
    }

    @Override
    public String toString() {
        if (this.toposortedNodes != null) {
            return this.toposortedNodes.toString();
        }
        return this.adjacencyList.toString();
    }

    /* begin: GETTERS and SETTERS */

    public int getPebblesLastTaken() {
        return this.activeState.getPebblesLastTaken();
    }

    /* end: GETTERS and SETTERS */

    /**
     * Generates all children for all nodes in graph. Assumes graph contains entry node.
     * The generation of the state graph uses Breath-First search to determine
     all possible states in the most efficent way possible. This allows for the
     generation of a graph with 1000 pebbles, with each state labelled,
     completed in 15 seconds. (With n=80 pebbles, the average time is 2 milliseconds over 250 trials!)
     The labelling (state success: win/lose) is implemented using a topologically sorted
     list, which is then iterated upon in the reverse order (using a descending iterator for efficiency).

     For the storing of the graph, there exists a Hashtable in GraphNIM which
     contains every node of the graph, representing a possible state of the game.
     Each node contains a adjacency list (storded as a LinkedList) which
     contains references to all neighbour nodes (states).
     This Hashtable implementation allows for constant time O(1) access to a vertex which
     increases the performance of the graph generation, by allowing for
     instant checking as to whether a node already exists in the graph.
     Unlike a LinkedList, which requires traversal over all nodes to verifiy whether it exists.
     */
    public void generateGraph() {
        // reset
        this.setNodesUnvisited();

        // copy list for searching
        HashtableNIM searchGraph = new HashtableNIM(this.adjacencyList);

        // for all unvisited nodes, decide children nodes
        searchGraph.values().stream().filter(current -> !current.isVisited()).forEach(
            this::breadthFirstGenerateGraphHelper
        );
    }

    /**
     * Sets all nodes in main list as unvisited
     */
    private void setNodesUnvisited() {
        for (NodeNIM current : this.adjacencyList.values()) {
            // set not visited
            current.setVisited(false);
        }
    }

    /**
     * Breadth First Search algorithm which generates all descending children (subsequent states).
     *
     * @param current home node
     */
    private void breadthFirstGenerateGraphHelper(NodeNIM current) {
        Queue<NodeNIM> queue = new LinkedList<>();

        current.setVisited(true);
        queue.add(current);

        while (!queue.isEmpty()) {
            NodeNIM childOfCurrent = queue.poll();

            // create adjacent nodes -- determine next states
            ListNIM adjacentNodes = this.calculateAdjacentNodes(childOfCurrent);

            // add child to main list
            this.add(childOfCurrent);

            // update neighbours
            childOfCurrent.addNeighbours(adjacentNodes);

            // iterate over each
            adjacentNodes.stream().filter(neighbour -> !neighbour.isVisited()).forEach(neighbour -> {
                neighbour.setVisited(true);
                queue.add(neighbour);
            });
        }
    }

    /**
     * Determines all neighbour nodes of the mainNode.
     * Starts from amount of pebbles on table, and the next amount allowed to take.
     * Then determines all the possible quantities possible
     * to take, i.e. moves possible, and for each, will create that state if it does not
     * already exist in the graph, else will fetch the state, and add it as a neighbour.
     *
     * @param mainNode current parent node
     * @return list of neighbours for mainNode
     */
    private ListNIM calculateAdjacentNodes(NodeNIM mainNode) {
        ListNIM neighbours = new ListNIM();
        int pebblesRemaining = mainNode.getPebblesRemaining();
        int pebblesAllowedToTake = mainNode.getPebblesAllowedToTake();

        // check if exit node -- has no neighbours
        if (mainNode.isDeadNode()) {
            // has no neighbours
            return neighbours;
        }

        // get neighbours
        for (int i = pebblesAllowedToTake; i > 0; i--) {
            int newPebblesAllowedToTake = Math.min((pebblesRemaining - i), i);

            // get if already exists
            String potentialKey = NodeNIM.createStateKeygen((pebblesRemaining - i), newPebblesAllowedToTake);
            if (this.adjacencyList.containsKey(potentialKey)) {
                // add as neighbour
                neighbours.add(this.adjacencyList.get(potentialKey));
            } else {
                // create new and add
                NodeNIM newNeighbour = new NodeNIM((pebblesRemaining - i), newPebblesAllowedToTake);

                // add neighbour of child to main list
                this.add(newNeighbour);

                // add as neighbour
                neighbours.add(newNeighbour);
            }
        }

        return neighbours;
    }

    /**
     * Adds node to main list if nonexistent.
     * Checks for key, thus achieved in O(1) time.
     *
     * @param newNode node to insert
     */
    public void add(NodeNIM newNode) {
        // add to main list
        if (!adjacencyList.containsKey(newNode.getKey())) {
            // add
            adjacencyList.put(newNode.getKey(), newNode);
        }
    }

    /**
     * Gets graph as topologically sorted list, if already determined,
     * will return previously calculated list to save computation.
     * During Topological sort, the next state to be inserted into the output,
     will be added to the start of a LinkedList, preventing the need for the
     list to be reversed once the sort completes -- reducing operational complexity.
     * @return list of nodes in graph, ordered topologically.
     */
    public ListNIM getToposortedNodes() {
        // lazy instantiation
        if (this.toposortedNodes == null) {
            // do sort
            this.toposortedNodes = this.fetchGraphAsToposortedList();
        }

        return this.toposortedNodes;
    }

    /**
     * Calculates the topological order of the main list of nodes in the graph.
     * For each existing entry point in graph, will start recursive helper call.
     * @return list of nodes in graph, ordered topologically
     */
    private ListNIM fetchGraphAsToposortedList() {
        // init
        this.toposortedNodes = new ListNIM();

        // reset
        this.setNodesUnvisited();

        // begin
        // start recursive call for each entry node
        // filtered out nodes already visited.
        this.adjacencyList.values().stream().filter(current -> !current.isVisited()).forEach(
            // start recursive call
            this::toposortHelper
        );

        return toposortedNodes;
    }

    /**
     * Recursive method to add all dependent neighbours to output list (main list) before adding the start (self) node.
     * Uses deep-first search approach to ensure all descending neighbours are added beforehand.
     * @param start node, or parent node to begin from
     */
    private void toposortHelper(NodeNIM start) {
        // set as visited
        start.setVisited(true);

        // for each neighbour of current not visited
        start.getNeighbours().stream().filter(current -> !current.isVisited()).forEach(
            // call recursive
            this::toposortHelper
        );

        // add node to start of list (toposort moves from end to start)
        toposortedNodes.addFirst(start);
    }

    /**
     * Set the entry node of the game.
     * This node must have no incoming edges.
     */
    public void start(NodeNIM entryNode) {
        // start game at entry node
        this.activeState = entryNode;
    }

    /**
     * Attempts to change from the current active state to the nextState if possible.
     * @param nextState the state to move into.
     * @throws IllegalMoveException thrown when next state is not a valid move.
     * @throws GameOverException thrown if move failed because of the game ended.
     */
    public void moveToState(NodeNIM nextState) throws IllegalMoveException, GameOverException {
        // no moves left
        if (this.noMovesLeft()) {
            // game over
            throw new GameOverException();
        }

        // check if valid -- current state has neighbour nextState
        if (this.activeState.getNeighbours().contains(nextState)) {
            this.changeActiveState(nextState);
        } else {
            throw new IllegalMoveException();
        }
    }

    /**
     * Returns true of there are no moves left. i.e. the active state is terminal.
     * @return boolean for any moves left.
     */
    public boolean noMovesLeft() {
        // game over
        return (this.activeState.isTerminal());
    }

    /**
     * Sets the active state to newState and calculates the amount of pebbles taken during move.
     * @param newState the state to move into.
     */
    private void changeActiveState(NodeNIM newState) {
        // calculate pebbles taken
        int pebblesTaken = 0;
        if (newState.getPebblesRemaining() < 1) {
            // all pebbles were taken
            pebblesTaken = this.activeState.getPebblesRemaining();
        } else {
            pebblesTaken = this.activeState.getPebblesRemaining() - newState.getPebblesRemaining();
        }
        newState.setPebblesLastTaken(pebblesTaken);

        // change
        this.activeState = newState;
    }

    /**
     * Moves to any possible next state. Updates active state to chosen state.
     * @throws GameOverException game has ended, no moves possible.
     */
    public void moveToAnyState() throws GameOverException {
        // no moves left
        if (this.noMovesLeft()) {
            // game over
            throw new GameOverException();
        }

        // moves to first move in list (effectively random)
        NodeNIM newState = this.activeState.getNeighbours().getFirst();
        this.changeActiveState(newState);
    }

    /**
     * Moves to a randomly selected state adjacent to the active state.
     * Shuffles possible moves before selection.
     * @throws GameOverException no moves exist
     */
    public void moveToRandomState() throws GameOverException {
        // no moves left
        if (this.noMovesLeft()) {
            // game over
            throw new GameOverException();
        }

        // shuffles and picks one
        ListNIM shuffledList = new ListNIM(this.activeState.getNeighbours());
        shuffledList.shuffle();

        NodeNIM newState = shuffledList.getFirst();

        this.changeActiveState(newState);
    }

    /**
     * Moves to a state if it is possible to take the amount of pebbles specified as pebblesToTake.
     * @param pebblesToTake amount of pebbles to take from table
     * @throws IllegalMoveException not able to take amount of pebbles from active state
     */
    public void moveToStateIfFound(int pebblesToTake) throws IllegalMoveException {
        // find the node
        for (NodeNIM currentNeighbourOfActive : this.activeState.getNeighbours()) {
            // check
            int expectedPebblesRemaining = this.activeState.getPebblesRemaining() - pebblesToTake;
            int expectedPebblesAllowedToTake = Math.min(expectedPebblesRemaining, pebblesToTake);

            String expectedKey = NodeNIM.createStateKeygen(expectedPebblesRemaining, expectedPebblesAllowedToTake);
            if (currentNeighbourOfActive.getKey().equals(expectedKey)) {
                // found now move to it
                this.changeActiveState(currentNeighbourOfActive);
                return;
            }
        }

        // not valid move
        throw new IllegalMoveException("Invalid move! Cannot find node to transition into after taking " + pebblesToTake + " pebbles.");
    }
}
