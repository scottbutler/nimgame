package me.scottbutler.NIMS;

import me.scottbutler.NIMS.exceptions.GameOverException;
import me.scottbutler.NIMS.exceptions.IllegalMoveException;
import me.scottbutler.NIMS.model.GraphNIM;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 * ...
 * ...
 */

public class NIMPlayer {
    protected final GraphNIM gameGraph;
    private String playerName;

    public NIMPlayer(GraphNIM gameStateGraph, String name) {
        this.playerName = name;
        this.gameGraph = gameStateGraph;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void move(int pebblesToTake) throws GameOverException, IllegalMoveException {
        int pebblesLastTaken = this.gameGraph.getPebblesLastTaken();

        // validate
        if (pebblesLastTaken != -1 && pebblesToTake > pebblesLastTaken) {
            throw new IllegalMoveException(
                "Invalid move! As I only took " + pebblesLastTaken + ((pebblesLastTaken > 1) ? " pebbles" : " pebble")
                    + " last time, you cannot take more than " + pebblesLastTaken + ".");
        } else if (pebblesToTake > this.gameGraph.getActiveState().getPebblesRemaining()) {
            throw new IllegalMoveException("Invalid move! You tried taking more pebbles than is on the table.");
        } else if (pebblesToTake < 1) {
            throw new IllegalMoveException("Invalid move! You tried taking 0 or less pebbles. Why!?");
        } else if (pebblesToTake > this.gameGraph.getActiveState().getPebblesAllowedToTake()) {
            throw new IllegalMoveException(
                "Invalid move! You tried taking more pebbles than is allowed. (" + this.gameGraph.getActiveState().getPebblesAllowedToTake() + ")");
        }

        // try to move
        this.gameGraph.moveToStateIfFound(pebblesToTake);
    }
}
