package me.scottbutler.demo.exceptions;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  In the event that the game moves into an invalid state,
 *  or becomes corrupted, the ResetGameException will
 *  either reset the game, or fully re-initialise the game.
 *  To commit a re-initialisation, throw an exception
 *  with "INIT" as the first parameter, and the message as the second parameter.
 */

public class ResetGameException extends Exception {
    private boolean fullInit = false;

    public ResetGameException(String initToo, String message) {
        super(message);

        if (initToo.equals("INIT")) {
            fullInit = true;
        }
    }

    public ResetGameException(String message) {
        super(message);
    }

    public ResetGameException() {
        super();
    }

    /* begin: GETTERS and SETTERS */

    public boolean doFullInit() {
        return fullInit;
    }

    /* end: GETTERS and SETTERS */
}
