package me.scottbutler.demo;

import me.scottbutler.NIMS.NIMGameboard;
import me.scottbutler.NIMS.exceptions.IllegalMoveException;
import me.scottbutler.demo.exceptions.ResetGameException;

import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  This class is the main demonstration class,
 *  in charge of interacting with the user,
 *  setting up the game, and controlling menu navigation.
 */

public class DemoGame {
    private NIMGameboard gameboard;

    private void printHeader() {
        System.out.println("*************************************************");
        System.out.println("*               Welcome to NIM                  *");
        System.out.println("*               by Scott Butler                 *");
        System.out.println("* --------------------------------------------  *");
        System.out.println("* INSTRUCTIONS:                                 *");
        System.out.println("* These are the rules for the NIM game:         *");
        System.out.println("* There are two players, A and B.               *");
        System.out.println("* There is a pile of pebbles on the table.      *");
        System.out.println("*                                               *");
        System.out.println("* Player A starts, taking some pebbles;         *");
        System.out.println("* he/she must take at least one pebble,         *");
        System.out.println("* and must leave at least one on the table.     *");
        System.out.println("*                                               *");
        System.out.println("* Player B then takes some pebbles; B must      *");
        System.out.println("* take at least one, but cannot take more than  *");
        System.out.println("* the number of pebbles player A took; and      *");
        System.out.println("* obviously player B cannot take more than the  *");
        System.out.println("* number of the pebbles left on the table.      *");
        System.out.println("*                                               *");
        System.out.println("* In general, a player can take any number of   *");
        System.out.println("* pebbles between 1 and the number that the     *");
        System.out.println("* previous player took, but obviously no more   *");
        System.out.println("* than the number of pebbles left on the table. *");
        System.out.println("*                                               *");
        System.out.println("* Player to take the last pebble wins.          *");
        System.out.println("*                                               *");
        System.out.println("* In this version, you are versing NIMbot.      *");
        System.out.println("*                                               *");
        System.out.println("*************************************************");
    }

    /**
     *  state machine for the main navigation of the game.
     *  in charge of interacting with the user, validating their input,
     *  performing all user actions, and displaying results.
     */
    private void mainMenu() {
        // until user ends game
        boolean exit = false;
        String state = "INIT";

        // main user input
        Scanner cin;

        do {
            // loop through menu

            // evaluate menu choice or setup game
            try {
                switch (state) {
                    case "INIT":
                        // start by asking user for initial pebble count
                        int _initialPebbleCount;
                        try {
                            System.out.print("Please enter the initial number of pebbles on the table: ");
                            cin = new Scanner(System.in);
                            _initialPebbleCount = cin.nextInt();
                            System.out.println("<"+_initialPebbleCount+">");
                        } catch (InputMismatchException e) {
                            throw new ResetGameException("INIT", "INVALID: You must enter a number.");
                        } finally {
                            // add new line
                            System.out.println();
                        }

                        // validate
                        if (_initialPebbleCount < NIMGameboard.getMIN_PEBBLE_COUNT()) {
                            throw new ResetGameException("INIT",
                                "INVALID: Initial pebble count cannot be less than " + NIMGameboard.getMIN_PEBBLE_COUNT() + ".");
                        } else {
                            // valid
                            // initialise game
                            this.gameboard = new NIMGameboard(_initialPebbleCount);

                            // go to loading of game
                            state = "GENERATE";
                        }

                        break;

                    case "RESET":
                        int initialPebbleCount = this.gameboard.getInitialPebbleCount();
                        // clear game
                        this.gameboard = new NIMGameboard(initialPebbleCount);

                        state = "GENERATE";
                        break;

                    case "MENU_999":
                    case "GENERATE":
                        System.out.print("Loading...");

                        this.gameboard.generateGraph();
                        this.gameboard.determineStateSuccess();

                        System.out.println("done");

                        // show pebbles on table
                        System.out.println(gameboard.boardStatus()+"\n");

                        // go to main menu
                        state = "MENU";

                        break;

                    case "MENU_666":
                    case "TEST": {
                        System.out.println("Testing...");

                        int trials = 250;
                        int testPebbleAmount = 80;

                        long sum1 = 0, sum2 = 0;

                        for (int i = 0; i < trials; i++) {
                            this.gameboard = new NIMGameboard(testPebbleAmount);

                            long lStartTime = new Date().getTime(); // start time

                            this.gameboard.generateGraph();

                            long lEndTimeGeneration = new Date().getTime();

                            this.gameboard.determineStateSuccess();

                            long lEndTime = new Date().getTime(); // end time

                            sum1 += lEndTimeGeneration - lStartTime;
                            sum2 += lEndTime - lStartTime;

                            // notify status
                            System.out.print("\r" + (i + 1) + " of " + trials + " trials completed");
                            System.out.flush();
                        }

                        long averageGeneration = sum1 / trials;
                        long averageTotal = sum2 / trials;

                        System.out.println(
                            "\nAverage Generation Time for " + trials + " trials with " + testPebbleAmount + " pebbles: " + (
                                (averageGeneration / 1000)
                                    % 60)
                                + " secs (" + averageGeneration + " milliseconds)");
                        System.out.println(
                            "Average Total Execution Time for " + trials + " trials with " + testPebbleAmount + " pebbles: " + ((averageTotal / 1000)
                                % 60)
                                + " secs (" + averageTotal + " milliseconds)");

                        System.out.println("...complete\n\n");

                        // reset game after testing changed pebble size (invalid)
                        state = "INIT";

                        break;
                    }

                    case "MENU": {
                        System.out.print("Choose an option from the menu\n"
                            + "1. Show NIM graph\n"
                            + "2. Show graph labels (winning/losing)\n"
                            + "3. Play the NIM game\n"
                            + "4. Change initial pebble count\n"
                            + "5. Exit\n"
                            + "Enter number: ");

                        // read in menu choice
                        try {
                            cin = new Scanner(System.in);
                            int _menuchoice = cin.nextInt();
                            System.out.println("<"+_menuchoice+">");

                            // go to menu choice if exists
                            state = "MENU_" + _menuchoice;
                        } catch (InputMismatchException e) {
                            System.out.println("INVALID: You must enter a number.");
                        } finally {
                            // add new line
                            System.out.println();
                        }

                        break;
                    }

                    case "MENU_1": {
                        System.out.println(
                            "NIM graph for n=" + this.gameboard.getInitialPebbleCount() + " pebble" + ((this.gameboard.getInitialPebbleCount() > 1) ?
                                "s " : " "));
                        System.out.println(this.gameboard.printGameGraph());

                        state = "MENU";
                        break;
                    }

                    case "MENU_2":
                        System.out.println(
                            "NIM graph for n=" + this.gameboard.getInitialPebbleCount() + " pebble" + ((this.gameboard.getInitialPebbleCount() > 1) ?
                                "s " : " "));
                        System.out.println(this.gameboard.getGameStateSuccessGraph());

                        state = "MENU";
                        break;

                    case "MENU_3": {
                        // begin playing game

                        // ask user for name
                        System.out.print("Enter your name: ");
                        cin = new Scanner(System.in);
                        String userPlayersName = cin.nextLine();
                        System.out.println("<"+userPlayersName+">\n");
                        System.out.println("\n");

                        System.out.println("Welcome " + userPlayersName + " you are versing the nifty " + this.gameboard.getBotsName() + "!\n");

                        // setup
                        String status_init;
                        try {
                            status_init = this.gameboard.boardStatus() + "\n";

                            this.gameboard.startGame(userPlayersName);
                        } catch (IllegalMoveException e) {
                            // system error
                            throw new ResetGameException("Error: AI bot took illegal move. Game restarting.");
                        }

                        boolean usersTurn = true;

                        // did AI go?
                        if (this.gameboard.AIbotWentFirst()) {
                            System.out.println(status_init);
                            System.out.println("I'll go first.\n");
                            System.out.println("I take " + this.gameboard.pebblesLastTaken() + "\n");
                        } else {
                            System.out.println("You go first " + this.gameboard.getUsersName() + ".\n");
                        }

                        while (!this.gameboard.gameover()) {
                            System.out.println(this.gameboard.boardStatus());

                            if (usersTurn) {

                                // until valid entry
                                for (boolean invalid = true; invalid; ) {
                                    // let user go
                                    System.out.print("How many will you take? ");

                                    try {
                                        cin = new Scanner(System.in);
                                        int pebblesToTake = cin.nextInt();

                                        System.out.println("<" + pebblesToTake + ">");

                                        this.gameboard.playUserMove(pebblesToTake);

                                        System.out.println();

                                        // valid move
                                        invalid = false;
                                    } catch (IllegalMoveException e) {
                                        System.out.println("\n" + e.getMessage());
                                        // try again
                                    } catch (InputMismatchException e) {
                                        System.out.println("\nInvalid move! That wasn't even a number.");
                                    }
                                }
                            } else {
                                // let bot go
                                try {
                                    this.gameboard.playBotMove();
                                } catch (IllegalMoveException e) {
                                    // system error
                                    throw new ResetGameException("Error: AI bot took illegal move during game. Game restarting.");
                                }
                                System.out.println("I take " + this.gameboard.pebblesLastTaken() + "\n");
                            }

                            // alternate turns
                            usersTurn = !usersTurn;
                        }

                        // output winner
                        System.out.println(this.gameboard.boardStatus() + "\n");
                        System.out.println("Gameover. The winner is " + this.gameboard.getWinner().getPlayerName() + "!\n\n");

                        // Reset game
                        state = "RESET";
                        break;
                    }

                    case "MENU_4":
                        // game will be reset
                        state = "INIT";
                        break;

                    case "MENU_5":
                    case "EXIT":
                        System.out.println("\nGoodbye.\n");
                        exit = true;
                        break;

                    case "MENU_333":
                    case "TOGGLE_AUTOBOT":
                        // toggles the cheating robot
                        this.gameboard.toggleAutobot();
                        // when off will take random move
                        if (this.gameboard.isBotCheating()) {
                            System.out.println("SETTING CHANGED: " + this.gameboard.getBotsName() + " is being nifty again, watch him cheat.\n");
                        } else {
                            System.out.println("SETTING CHANGED: " + this.gameboard.getBotsName() + " wont cheat anymore.\n");
                        }

                        state = "MENU";
                        break;

                    default:
                        // check if system error or user input error
                        if (state.startsWith("MENU_")) {
                            // invalid menu option
                            System.out.println("INVALID: That menu choice does not exist.");
                            state = "MENU";
                        } else {
                            // system error
                            System.out.println("ERROR: invalid state choice, game exiting.");
                            state = "EXIT";
                        }
                        break;
                }
            } catch (ResetGameException e) {
                System.out.println(e.getMessage());
                if (e.doFullInit()) {
                    state = "INIT";
                } else {
                    state = "RESET";
                }
            }
        } while (!exit);
    }

    /**
     * entry point, prints standard header, and enters main state.
     */
    public void run() {
        this.printHeader();
        this.mainMenu();
    }
}
