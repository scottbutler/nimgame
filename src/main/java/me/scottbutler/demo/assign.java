package me.scottbutler.demo;

/**
 * author: Scott Butler
 * studentID: c3165874
 * contactEmail: c3165874@uon.edu.au
 * institution: University of Newcastle, Australia
 * createdDate: SEP/2015
 * projectName: Assignment 1 NIMgame
 * courseCode: COMP2230
 * programDescription:
 *  A program to demonstrate the NIM game
 *  with a user playing against NIMbot (the always winning bot).
 * classDescription:
 *  Purely a entry point to create an instance of the game, out-of-static area.
 */

public class assign {
    public static void main(String[] args) {
        DemoGame maindemo = new DemoGame();
        maindemo.run();
    }
}
